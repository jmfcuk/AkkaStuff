﻿using Akka.Actor;
using Akka.Configuration;
using System;

namespace System2
{
	class Program
	{
		private const string _SYSTEM_NAME = "TestSystem2";

		static void Main(string[] args)
		{
			//var config = ConfigurationFactory.ParseString(@"
			//akka {
			//	actor {
			//		provider = "Akka.Remote.RemoteActorRefProvider, Akka.Remote"
			//		}
			//	remote {
			//		helios.tcp {
			//			port = 8090
			//				hostname = 127.0.0.1
			//			}
			//	}
			//}
			//	");

			using (ActorSystem actorSystem = ActorSystem.Create(_SYSTEM_NAME))
			{
				var actor =
					actorSystem.ActorSelection("akka.tcp://TestSystem1@127.0.0.1:8080/user/TestActor1");

				actor.Tell("Testing from system 2");

				string s = string.Empty;

				do
				{
					s = Console.ReadLine();

					actor.Tell(s);

				} while (s != "q");

				actorSystem.Terminate();

				actorSystem.WhenTerminated.Wait();
			}
		}
	}
}

