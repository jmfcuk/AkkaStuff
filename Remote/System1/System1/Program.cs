﻿using Akka.Actor;
using Shared;
using System;

namespace System1
{
	class Program
	{
		private const string _SYSTEM_NAME = "TestSystem1";

		static void Main(string[] args)
		{
			using (ActorSystem actorSystem = ActorSystem.Create(_SYSTEM_NAME))
			{
				IActorRef iar = actorSystem.ActorOf<TestActor>("TestActor1");

				Console.ReadLine();

				actorSystem.Terminate();

				actorSystem.WhenTerminated.Wait();
			}
		}
	}
}
