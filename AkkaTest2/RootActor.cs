﻿using Akka.Actor;
using Akka.Event;
using Akka.Routing;
using System.Collections.Generic;

namespace AkkaTest2
{
	public sealed class RootActor : UntypedActor
	{
		private readonly ILoggingAdapter _log = null;

		private IActorRef _childGroup;

		public RootActor(IActorRef actorGroup)
		{
			_childGroup = actorGroup;
		}

		public RootActor(IEnumerable<IActorRef> childActors)
		{
			List<string> paths = new List<string>();

			foreach (IActorRef actor in childActors)
				paths.Add(actor.Path.ToString());

			_childGroup = Context.ActorOf(Props.Empty.WithRouter(new RoundRobinGroup(paths)));
		}

		public RootActor(int childCount)
		{
			_log = Logging.GetLogger(Context);

			List<string> paths = new List<string>();

			for (int i = 0; i < childCount; i++)
				paths.Add(Context.ActorOf<ChildActor>($"ChildActor{i}").Path.ToString());
			
			_childGroup = Context.ActorOf(Props.Empty.WithRouter(new RoundRobinGroup(paths)));
		}

		protected override void OnReceive(object message)
		{
			if (message is Messages.ResponseMessage)
				_log.Info($"Response: {(message as Messages.ResponseMessage).Msg}");
			else
			{
				_log.Info($"RootActor : {message}");

				Messages.DoWorkMessage msg1 = new Messages.DoWorkMessage("do work message");
				_childGroup.Tell(msg1);

				Messages.DelegateWorkMessage msg2 = new Messages.DelegateWorkMessage("delegate work message");
				_childGroup.Tell(msg2);
			}
		}
	}
}
