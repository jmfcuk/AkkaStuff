﻿using Akka.Actor;
using System;

namespace AkkaTest2
{
	//akka.tcp://TestActorSystem@localhost:9001/user/root

	class Program
    {
		private static ActorSystem _actorSystem;
		private const string _SYSTEM_NAME = "TestActorSystem";
		private const int _CHILD_COUNT = 4;
		        
        static void Main(string[] args)
        {
			_actorSystem = ActorSystem.Create(_SYSTEM_NAME);

			IActorRef root =
				_actorSystem.ActorOf(
					Props.Create(() => new RootActor(_CHILD_COUNT), 
									   new RootActorSupervisorStrategy()), "RootActor");
			ForwardInput(root);

			_actorSystem.Terminate();

			_actorSystem.WhenTerminated.Wait();
        }

		private static void ForwardInput(IActorRef iar)
		{
			string s = string.Empty;
			while (0 != string.Compare(s, "quit", true) &&
				   0 != string.Compare(s, "q", true))
			{
				s = Console.ReadLine();

				iar.Tell(s);
			}
		}
    }
}


