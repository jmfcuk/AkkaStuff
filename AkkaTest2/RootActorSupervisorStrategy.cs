﻿using Akka.Actor;
using System;

namespace AkkaTest2
{
	public class RootActorSupervisorStrategy : AllForOneStrategy
	{
		public RootActorSupervisorStrategy() : base(
				maxNrOfRetries: 1,
				withinTimeRange: TimeSpan.FromSeconds(1),
				localOnlyDecider: x =>
				{
					return Directive.Stop;

					//return Directive.Resume;

					//return Directive.Escalate;

					//return Directive.Stop;

					//Directive.Restart;
				})
				{
				}
	}
}
