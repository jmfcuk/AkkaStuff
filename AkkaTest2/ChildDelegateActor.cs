﻿using System;
using Akka.Actor;
using Akka.Event;

namespace AkkaTest2
{
	class ChildDelegateActor : UntypedActor
	{
		private readonly ILoggingAdapter _log = null;

		public ChildDelegateActor()
		{
			_log = Logging.GetLogger(Context);
		}

		protected override void OnReceive(object message)
		{
			_log.Info($"ChildDelegateActor : {Self.Path.Name} : {message}");
		}
	}
}
