﻿using Akka.Actor;
using Akka.Event;
using Akka.Routing;
using System;

namespace AkkaTest2
{
	class ChildActor : UntypedActor
	{
		private readonly ILoggingAdapter _log = null;

		private IActorRef _delegateActors;

		public ChildActor()
		{
			_log = Logging.GetLogger(Context);

			Props pRRPool = new RoundRobinPool(4)
				.Props(
				Props.Create(() => new ChildDelegateActor()));

			_delegateActors = Context.ActorOf(pRRPool);
		}

		protected override void OnReceive(object message)
		{
			_log.Info($"ChildActor : {Self.Path.Name} : {message}");

			Sender.Tell(new Messages.ResponseMessage($"ChildActor : {Self.Path.Name} : {(message as Messages.Message).Msg}"));

			if (message is Messages.DoWorkMessage)
				;
			else if (message is Messages.DelegateWorkMessage)
				_delegateActors.Tell("some delgated work");
			else
				throw new Exception("Bang!");
		}

		protected override SupervisorStrategy SupervisorStrategy()
		{
			return new OneForOneStrategy(
				maxNrOfRetries: 5,
				withinTimeRange: TimeSpan.FromSeconds(1),
				localOnlyDecider: x =>
				{
					//return Directive.Stop;

					//return Directive.Resume;

					//return Directive.Escalate;

					//return Directive.Stop;

					return Directive.Restart;
				});
		}
	}
}


