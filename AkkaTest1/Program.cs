﻿using Akka.Actor;
using System;

namespace AkkaTest1
{
	//akka.tcp://MyActorSystem@localhost:9001/user/a1/b2

	class Program
    {
		private static ActorSystem _actorSystem;
		private const string _SYSTEM_NAME = "TestActorSystem";
		private const int _CHILD_COUNT = 4;
		private const int _POKE_COUNT = 1;
		        
        static void Main(string[] args)
        {
			_actorSystem = ActorSystem.Create(_SYSTEM_NAME);

			IActorRef root =
				_actorSystem.ActorOf(
					Props.Create(() => new RootActor(_CHILD_COUNT), 
									   new RootActorSupervisorStrategy()), "RootActor");

			//for(int x = 0; x < _POKE_COUNT; x++)
			//	root.Tell($"Poke {x}");
			string s = string.Empty;
			while (0 != string.Compare(s, "exit", true))
			{
				s = Console.ReadLine();

				root.Tell(s);
			}

			//Console.ReadLine();

			_actorSystem.Terminate();

			_actorSystem.WhenTerminated.Wait();
        }
    }
}


