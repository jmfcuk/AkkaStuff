﻿using Akka.Actor;
using Akka.Event;
using Akka.Routing;
using System;

namespace AkkaTest1
{
	public sealed class RootActor : UntypedActor
	{
		private readonly ILoggingAdapter _log = null;

		private IActorRef _childPool;

		public RootActor(int childCount)
		{
			_log = Logging.GetLogger(Context);

			Props pRRPool = new RoundRobinPool(childCount)
				.Props(
				Props.Create<ChildActor>());

			_childPool = Context.ActorOf(pRRPool);
		}

		protected override void OnReceive(object message)
		{
			if (message is Messages.ResponseMessage)
				_log.Info($"Response: {(message as Messages.ResponseMessage).Msg}");
			else
			{
				_log.Info($"RootActor : {message}");

				Messages.DoWorkMessage msg1 = new Messages.DoWorkMessage("do work message");
				_childPool.Tell(msg1);

				Messages.DelegateWorkMessage msg2 = new Messages.DelegateWorkMessage("delegate work message");
				_childPool.Tell(msg2);
			}
		}
	}
}
