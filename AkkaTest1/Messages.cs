﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaTest1
{
	public class Messages
	{
		public class Message
		{
			public string Msg { get; private set; }

			public Message(string msg)
			{
				Msg = msg;
			}
		}

		public class DoWorkMessage : Message
		{
			public DoWorkMessage(string msg) : base(msg) { }
		}

		public class DelegateWorkMessage : Message
		{
			public DelegateWorkMessage(string msg) : base(msg) { }
		}


		public class ResponseMessage : Message
		{
			public ResponseMessage(string msg) : base(msg) { }
		}
	}
}
