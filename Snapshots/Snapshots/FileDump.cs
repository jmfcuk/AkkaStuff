﻿using System.Configuration;
using System.IO;

namespace Snapshots
{
	static class FileDump
	{
		private static readonly string _fileDir =
			ConfigurationManager.AppSettings["outputFolder"];

		public static void Dump(string filename, string content)
		{
			if (!Directory.Exists(Path.GetDirectoryName(_fileDir)))
				Directory.CreateDirectory(_fileDir);

			string path = Path.Combine(_fileDir, filename);

			File.WriteAllText(path, content);
		}
	}
}
