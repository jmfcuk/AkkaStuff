﻿using System;

namespace Snapshots
{
	public class TestMessage
    {
		public readonly int Ordinal;
        public readonly string Value;
        
        public TestMessage(int ordinal, string value)
        {
			Ordinal = ordinal;
            Value = value;
        }
    }

	public class FailedMessage
	{
		public readonly TestMessage FailedTestMessage;
		public readonly Exception Ex;

		public FailedMessage(Exception e, TestMessage msg)
		{
			Ex = e;
			FailedTestMessage = msg;
		}
	}
}
