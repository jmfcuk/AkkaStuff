﻿using Akka.Actor;
using Akka.Event;
using System;

namespace Snapshots
{
	class ChildActor : ReceiveActor
	{
		private readonly ILoggingAdapter _log = Logging.GetLogger(Context);

		private IActorRef _failedMsgActor;

		public ChildActor()
		{
			_failedMsgActor =
				Context.ActorOf<FailedMessageActor>("FailedMessageActor");

			Receive<TestMessage>(msg =>
			{
				_log.Info($"ChildActor Command Received {msg.Value}");

				if (msg.Ordinal == 5)
				{
					string s = new string('a', int.MaxValue);
				}

				FileDump.Dump($"msg-{msg.Ordinal}.txt",
							  msg.Ordinal + "\t: " + msg.Value);

				_log.Info($"ChildActor Command Processed {msg.Value}");
			});

			Receive<FailedMessage>(msg =>
			{
				_failedMsgActor.Tell(msg);
			});
		}
    
		protected override void PreRestart(Exception reason, object message)
		{
			_log.Info("ChildActor::PreRestart()");

			TestMessage msg = message as TestMessage;
			
			_log.Error($"ChildActor::PreRestart() - message \"{msg.Ordinal}\t: {msg.Value}\" FAILED.");
			_log.Error($"ChildActor::PreRestart() - exception \"{reason.Message}\"");

			Self.Tell(new FailedMessage(reason, msg));

			base.PreRestart(reason, message);
		}
	}
}


