﻿using Akka.Actor;
using System;

namespace Snapshots
{
	class Program
	{
		private const string _SYSTEM_NAME = "TestSystem";

		static void Main(string[] args)
		{
			using (ActorSystem acsys = ActorSystem.Create(_SYSTEM_NAME))
			{
				IActorRef iar = acsys.ActorOf<RootActor>("RootActor1");

				Console.WriteLine("Enter to start...");
				Console.ReadLine();

				for (int x = 0; x < 10; x++)
					iar.Tell(new TestMessage(x, $"Test{x}"));

				//acsys.Stop(iar);

				//iar.GracefulStop(TimeSpan.FromSeconds(60)).Wait();

				//iar.Tell(PoisonPill.Instance);

				//iar.Tell(new TestMessage(9999, "Last Message"));

				//var dlq = acsys.DeadLetters;

				Console.WriteLine("Enter to terminate system...");
				Console.ReadLine();

				acsys.Terminate().Wait();

				Console.WriteLine("Enter to exit...");
				Console.ReadLine();
			}
		}
	}
}


