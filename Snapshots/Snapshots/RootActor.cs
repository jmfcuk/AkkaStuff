﻿using Akka.Actor;
using Akka.Event;
using System;

namespace Snapshots
{
	public class RootActor : ReceiveActor, ILogReceive
	{
		private readonly ILoggingAdapter _log = Logging.GetLogger(Context);

		private readonly IActorRef _iar;

		public RootActor()
		{
			_iar = Context.ActorOf<ChildActor>("ChildActor");

			Receive<TestMessage>(msg =>
			{
				_log.Info($"RootActor Received {msg.Value}");

				_iar.Tell(msg);
			});
		}
		
		protected override void PreStart()
		{
			_log.Info("RootActor::PreStart()");

			base.PreStart();
		}

		protected override void PreRestart(Exception reason, object message)
		{
			_log.Info("RootActor::PreRestart()");

			base.PreRestart(reason, message);
		}

		protected override SupervisorStrategy SupervisorStrategy()
		{
			_log.Info("RootActor::SupervisorStrategy()");

			return new AllForOneStrategy(
				maxNrOfRetries: 10,
				withinTimeRange: TimeSpan.FromSeconds(30),
				localOnlyDecider: x =>
				{
					return Directive.Restart;
				});
		}
	}
}

