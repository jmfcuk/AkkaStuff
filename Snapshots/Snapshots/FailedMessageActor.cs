﻿using Akka.Actor;
using Akka.Event;

namespace Snapshots
{
	class FailedMessageActor : ReceiveActor
	{
		private readonly ILoggingAdapter _log = Logging.GetLogger(Context);

		public FailedMessageActor()
		{
			Receive<FailedMessage>(msg =>
			{
				_log.Info($"FailedMessagesActor Received {msg.FailedTestMessage.Value}");

				FileDump.Dump($"FAILED-msg-{msg.FailedTestMessage.Ordinal}.txt",
							  msg.FailedTestMessage.Ordinal + 
							  $"\t: {msg.FailedTestMessage.Value}" +
							  $"\t: {msg.Ex}");
			});
		}
	}
}


