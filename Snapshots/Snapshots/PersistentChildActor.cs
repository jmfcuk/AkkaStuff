﻿using Akka.Event;
using Akka.Persistence;
using System;
using System.Collections.Generic;

namespace Snapshots
{
	class PersistentChildActor : ReceivePersistentActor
	{
		private readonly ILoggingAdapter _log = Logging.GetLogger(Context);

		private List<TestMessage> _msgs = new List<TestMessage>();
		private int _currentSnapshotBufferCount = 0;

		private string _pId = "PersistentChildActor";
		public override string PersistenceId
		{
			get
			{
				return _pId;
			}
		}

		public PersistentChildActor()
		{
			Command<TestMessage>(msg => Persist(msg, s =>
			{
				_log.Info($"PersistentChildActor Command Received {msg.Value}");

				//_msgs.Add(s);

				//if (++_currentSnapshotBufferCount % 2 == 0)
				//SaveSnapshot(msg);

				//if (msg.Ordinal % 2 == 0)
				//	throw new Exception("Bang!");

				FileDump.Dump($"p-msg-{msg.Ordinal}.txt",
							  msg.Ordinal + "\t:\t" + msg.Value);

				_log.Info($"PersistentChildActor Command Processed {msg.Value}");
			}));

			Command<SaveSnapshotSuccess>(msg => {

				_log.Info($"PersistentChildActor Received SaveSnapshotSuccess. {msg.Metadata.PersistenceId}");
				
				//DeleteSnapshots(SnapshotSelectionCriteria.Latest);
				//DeleteMessages(msg.Metadata.SequenceNr);
			});

			Command<SaveSnapshotFailure>(msg => {
				_log.Error($"PersistentChildActor Failed to save snapshot. {msg.Cause.Message}");
			});

			Recover<TestMessage>(msg =>
			{
				var self = Self;
				_log.Info($"PersistentChildActor Recover<TestMessage> Received {msg.Value}");

				self.Tell(msg, self);

				_log.Info($"PersistentChildActor  Processed {msg.Value}");
			});

			Recover<SnapshotOffer>(offer =>
			{
				_log.Info("PersistentChildActor Recover<SnapshotOffer> Received SnapshotOffer");

				List<TestMessage> tms = offer.Snapshot as List<TestMessage>;

				if (tms != null)
				{
					foreach (TestMessage tm in tms)
					{
						_log.Info(
							$"PersistentChildActor Recover<SnapshotOffer> Received Snapshot {tm.Ordinal}\t: {tm.Value }"); ;
					}
				}
			});
		}

		protected override void PreStart()
		{
			_log.Info("PersistentChildActor::PreStart()");

			base.PreStart();
		}

		protected override void PreRestart(Exception reason, object message)
		{
			_log.Info("PersistentChildActor::PreRestart()");

			TestMessage msg = message as TestMessage;

			_log.Error($"PersistentChildActor::PreRestart() - message \"{msg.Ordinal}\t: {msg.Value}\" FAILED.");

			base.PreRestart(reason, message);
		}
	}
}


