﻿using Akka.Actor;
using Messages;
using System;
using System.IO;

namespace Client
{
    class Program
    {
        private const string _SYSTEM_NAME = "ClientSystem";

        static void Main(string[] args)
        {
            Console.WriteLine("<Enter> to send message, 'q' to quit");

            using (ActorSystem actorSystem = ActorSystem.Create(_SYSTEM_NAME))
            {
                var broker =
                    actorSystem.ActorSelection("akka.tcp://GatewaySystem@127.0.0.1:8080/user/BrokerActor");

                string xml = File.ReadAllText(@"C:\_dev\AkkaStuff\ProdPush\Data\Prods.xml");

                string s = string.Empty;

                do
                {
                    s = Console.ReadLine();

                    if (0 != string.Compare(s, "q", true))
                        broker.Tell(new BrokerRequestMessage(xml));

                } while (s != "q");

                actorSystem.Terminate();

                actorSystem.WhenTerminated.Wait();

                Console.WriteLine("Enter to exit...");
                Console.ReadLine();
            }
        }
    }


    //class Program
    //{
    //    private const string _SYSTEM_NAME = "ClientSystem";

    //    static void Main(string[] args)
    //    {
    //        Console.WriteLine("<Enter> to send message, 'q' to quit");

    //        using (ActorSystem actorSystem = ActorSystem.Create(_SYSTEM_NAME))
    //        {
    //            var broker =
    //                actorSystem.ActorSelection("akka.tcp://GatewaySystem@127.0.0.1:8080/user/BrokerActor");

    //            string xml = File.ReadAllText(@"C:\_dev\AkkaStuff\ProdPush\Data\Prods.xml");

    //            string s = string.Empty;

    //            do
    //            {
    //                s = Console.ReadLine();

    //                if (0 != string.Compare(s, "q", true))
    //                    broker.Tell(new BrokerRequestMessage(xml));

    //            } while (s != "q");

    //            actorSystem.Terminate();

    //            actorSystem.WhenTerminated.Wait();

    //            Console.WriteLine("Enter to exit...");
    //            Console.ReadLine();
    //        }
    //    }
    //}
}
