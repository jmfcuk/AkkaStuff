﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messages
{
	public class PPMessage
	{
		public Guid Id { get; }

		public string Msg { get; }

		public PPMessage(string msg)
		{
			Id = Guid.NewGuid();
			Msg = msg;
		}
	}
}
