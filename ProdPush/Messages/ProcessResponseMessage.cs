﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messages
{
	public class ProcessResponseMessage : PPMessage
	{
		public ProcessResponseMessage(string msg) : base(msg) { }
	}
}
