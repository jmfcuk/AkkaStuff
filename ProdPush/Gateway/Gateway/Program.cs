﻿using Actors;
using Akka.Actor;
using Topshelf;

namespace Gateway
{
	class Program
	{
		static void Main(string[] args)
		{
			const string _SERVICE_NAME = "GateWay Service";

			HostFactory.Run(x =>
			{
				x.Service<GatewayService>(s =>
				{
					s.ConstructUsing(n => new GatewayService());
					s.WhenStarted(service => service.Start());
					s.WhenStopped(service => service.Stop());
				});

				x.SetServiceName(_SERVICE_NAME);
				x.RunAsLocalSystem();
			});
		}

		public class GatewayService
		{
			private const string _SYSTEM_NAME = "GatewaySystem";

			private ActorSystem _system;

			public void Start()
			{
				_system = ActorSystem.Create(_SYSTEM_NAME);

				IActorRef iar = _system.ActorOf(
					Props.Create(() => new BrokerActor(8)), "BrokerActor");
			}

			public void Stop()
			{
				_system.Terminate().Wait();
			}

			//public async void Stop()
			//{
			//	await _system.Terminate();
			//}
		}
	}
}


