﻿using Actors.Exceptions;
using Akka.Actor;
using Akka.Routing;
using Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace Actors
{
	public class BrokerActor : UntypedActor
	{
		private IActorRef _processingPool;

		public BrokerActor()
			: this(8)
		{ }

		public BrokerActor(int maxPoolSize)
		{
			Props rrPool = new RoundRobinPool(maxPoolSize)
				.Props(
				Props.Create<ProcessActor>());

			_processingPool = Context.ActorOf(rrPool);
		}

		public BrokerActor(IActorRef pool)
		{
			_processingPool = pool;
		}

		protected override void OnReceive(object message)
		{
			if (message != null && message is BrokerRequestMessage)
			{
				BrokerRequestMessage brm = message as BrokerRequestMessage;
				Console.WriteLine($"{Self.Path.ToString()} received {brm.Msg}");

				ValidateAndForwardMessage(brm);
			}
			else if (message != null && message is ProcessResponseMessage)
			{
				ProcessResponseMessage brm = message as ProcessResponseMessage;

				Console.WriteLine($"{Self.Path.ToString()} received response for {brm.Msg}");
			}
			else
				throw (new ArgumentException("Invald Message", "message"));
		}

		protected override SupervisorStrategy SupervisorStrategy()
		{
			return new OneForOneStrategy(
				maxNrOfRetries: 10,
				withinTimeRange: TimeSpan.FromSeconds(30),
				localOnlyDecider: x =>
				{
					if (x is ArgumentException)
					{
						Console.WriteLine($"{x.Message}");

						return Directive.Restart;
					}
					else if (x is XmlValidationException)
					{
						return Directive.Restart;
					}

					return Directive.Resume;
				});
		}

		private void ForwardMessage(BrokerRequestMessage msg)
		{
			XDocument doc = XDocument.Parse(msg.Msg);

			var xmls = doc.Root.Elements().ToArray();

			foreach (var e in xmls)
			{
				XDocument xdoc = new XDocument();

				_processingPool.Tell(new ProcessRequestMessage(e.ToString()));
			}
		}

		private void ValidateAndForwardMessage(BrokerRequestMessage msg)
		{
			XmlReaderSettings settings = new XmlReaderSettings();
			settings.ValidationType = ValidationType.Schema;
			settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
			settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessSchemaLocation;
			settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;

			List<ValidationEventArgs> vargs = new List<ValidationEventArgs>();

			settings.ValidationEventHandler += new ValidationEventHandler(
				(sender, args) =>
					{
						vargs.Add(args);
					}
				);

			using (XmlReader reader = XmlReader.Create(new StringReader(msg.Msg), settings))
			{
				while (reader.Read());

				if (vargs.Count > 0)
					throw new XmlValidationException(vargs);
				else
					ForwardMessage(msg);
			}
		}
	}
}


