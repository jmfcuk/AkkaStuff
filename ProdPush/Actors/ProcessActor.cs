﻿using Akka.Actor;
using Messages;
using System;

namespace Actors
{
	public class ProcessActor : UntypedActor
	{
		protected override void OnReceive(object message)
		{
            if (message != null && message is ProcessRequestMessage)
            {
                Console.WriteLine($"{Self.Path.ToString()} received " +
                    $"{(message as ProcessRequestMessage).Msg }");

                Sender.Tell(new ProcessResponseMessage($"Got {(message as ProcessRequestMessage).Id}"));
            }
            else
                throw (new ArgumentException("Invald Message", "message"));
		}
	}
}
