﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace Actors.Exceptions
{
	public class XmlValidationException  : Exception
	{
		public List<ValidationEventArgs> ValidationArgs { get; }

		public XmlValidationException(List<ValidationEventArgs> vargs) : base()
		{
			ValidationArgs = vargs;
		}
	}
}
